package com.example.homework14

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.homework14.HttpRequest.USERS
import kotlinx.android.synthetic.main.activity_create.*

class CreateActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create)
        init()
    }

    private fun init() {
        createButton.setOnClickListener {
            create()
        }
    }

    private fun create() {

        val parameters = mutableMapOf<String, String>()
        parameters["name"] = nameEditText.text.toString()
        parameters["job"] = jobEditText.text.toString()
        HttpRequest.postRequest(USERS, parameters, object : CustomCallback {

            override fun onResponse(response: String, message: String) {
                Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT)
                    .show()
                resultTextView.text = message
            }

            override fun onFailure(response: String, message: String) {
                Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT)
                    .show()
                resultTextView.text = message

            }

        })
    }

}
