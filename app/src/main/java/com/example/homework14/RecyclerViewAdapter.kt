package com.example.homework14

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_recyclerview_layout.view.*

class RecyclerViewAdapter(private val items: MutableList<Data>, private val itemOnClick: ItemOnClick) :
    RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_recyclerview_layout,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        return holder.onBind()
    }

    override fun getItemCount() = items.size

    private lateinit var model: Data

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),  View.OnClickListener {
        fun onBind() {
            model = items[adapterPosition]
            itemView.idTextView.text = model.id.toString()
            itemView.nameTextView.text = model.name
            itemView.yearTextView.text = model.year.toString()
            itemView.colorTextView.text = model.color
            itemView.pantoneValueTextView.text =model.pantoneValue
            itemView.setOnClickListener(this)

        }

        override fun onClick(v: View?) {
            itemOnClick.onClick(adapterPosition)
        }
    }

    interface ItemOnClick {
        fun onClick(position:Int)
    }
}