package com.example.homework14

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.homework14.HttpRequest.UNKNOWN
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private val itemsList = mutableListOf<Data>()
    private lateinit var adapter: RecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        request(UNKNOWN)
    }


    private fun init() {
        adapter = RecyclerViewAdapter(itemsList,object: RecyclerViewAdapter.ItemOnClick{
            override fun onClick(position: Int) {
                val intent = Intent(applicationContext, SingleUserActivity::class.java)
                startActivity(intent)
            }
        })
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter
        loadingTextView.visibility = View.VISIBLE

        swipeRefreshLayout.setOnRefreshListener {
            swipeRefreshLayout.isRefreshing = true
            refresh()

            Handler().postDelayed({
                swipeRefreshLayout.isRefreshing = false
                request(UNKNOWN)

            }, 2000)
        }

    }

    private fun refresh() {
        itemsList.clear()
        adapter.notifyDataSetChanged()
        loadingTextView.visibility = View.VISIBLE
    }

    private fun request(path: String) {
        HttpRequest.getRequest(path, object : CustomCallback {
            override fun onFailure(response: String, message: String) {
                Toast.makeText(applicationContext, "Error while loading page", Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(response: String, message: String) {
                val itemModel = Gson().fromJson(response,ItemModel:: class.java)
                for(item in itemModel.data){
                    itemsList.add(item)
                    adapter.notifyItemInserted(itemsList.size-1)
                }
                adapter.notifyDataSetChanged()
                loadingTextView.visibility = View.GONE
            }


        })
    }


}
