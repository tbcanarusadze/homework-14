package com.example.homework14

import com.google.gson.annotations.SerializedName

class ItemModel {
    var page: Int = 0
    @SerializedName("per_page")
    var perPage: Int = 0
    var total: Int = 0
    @SerializedName("total_pages")
    var totalPages: Int = 0
    var data: MutableList<Data> = mutableListOf()


    class Add {
        var company: String = ""
        var url: String = ""
        var text: String = ""

    }

}
class Data {

    var id: Int = 0
    var name: String = ""
    var year: Int = 0
    var color: String = ""
    @SerializedName("pantone_value")
    var pantoneValue: String = ""
}










