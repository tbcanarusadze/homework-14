package com.example.homework14

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.homework14.HttpRequest.UNKNOWN2
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_single_user.*

class SingleUserActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_single_user)
        init()
    }
    private fun init(){
        request(UNKNOWN2)
        createActivityButton.setOnClickListener {
            val intent = Intent(applicationContext, CreateActivity::class.java)
            startActivity(intent)
        }
    }

    private fun request(path: String) {
        HttpRequest.getRequest(path, object : CustomCallback {
            override fun onFailure(response: String, message: String) {
                Toast.makeText(applicationContext, "Error while loading page", Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(response: String, message: String) {
                val dataModel = Gson().fromJson(response,Data:: class.java)
                    singleIdTextView.text = dataModel.id.toString()
                    singleNameTextView.text = dataModel.name
                    singleYearTextView.text = dataModel.year.toString()
                    singleColorTextView.text = dataModel.color
                    singlePantoneTextView.text = dataModel.pantoneValue
                }


            })

    }

}

