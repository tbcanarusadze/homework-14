package com.example.homework14

import retrofit2.http.Body

interface CustomCallback {
    fun onFailure(response: String, message: String)
    fun onResponse(response: String, message: String)
}